#!/usr/bin/env python3

"""
List the existing labels in the given input file.
"""

import sys
import numpy as np

labels_file = (sys.argv[1] if len(sys.argv) > 1 else
               'data/umap_subsampling_1_10_umap_labels.csv')

# The file looks like:
#
# labels
# host-associated:plant host:aereal
# host-associated:animal host:skin
# aquatic:freshwater
# host-associated:plant host:aereal
# ...

y_labels = np.loadtxt(labels_file, delimiter=',', skiprows=1, dtype=str)

labels = sorted(set(y_labels))

print(f'Labels (and number of entries) present in {labels_file} :')
for label in labels:
    print('  %5d -- %s' % (sum(y_labels == label), label))
