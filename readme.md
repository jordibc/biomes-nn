# Neural network to study biomes

This program trains a simple neural network to recognize from which
biome a given sample is coming.

But mostly we wrote it to have an example of how to do an analysis
like this. The idea is to have a simple code that is general enough
for us to extend in different directions in the future.


## Data samples

Each sample is represented by a list of numbers -- in our case, a list
of [umap](https://umap-learn.readthedocs.io/) values, coming from a
pre-analysis of a list of presence (1) or ausence (0) of a large list
of chemoreceptors. There is an example list in
`data/umap_subsampling_1_10_umap_output.csv`, and the labels (biomes)
that correspond to each entry in
`data/umap_subsampling_1_10_umap_labels.csv`.

The example labels and data files look like:

```
labels
host-associated:plant host:aereal
host-associated:animal host:skin
aquatic:freshwater
host-associated:plant host:aereal
...

umap_1,umap_2,umap_3,umap_4,umap_5,umap_6
14.752,-5.809,18.367,12.811,10.35,7.6644
15.415,-5.270,17.591,13.164,10.875,8.356
15.505,-6.691,18.853,13.215,9.072,7.8103
15.412,-4.039,18.141,13.371,10.829,7.627
...
```

for a total of 4595 samples.


## Running

To see all the available options, you can run:

```sh
$ ./biomes_nn.py --help
```

Example of run:

```
$ ./biomes_nn.py --quiet --epochs 300
Correct predictions:
    114 / 163   (69.9 %) -- aquatic:freshwater
    167 / 205   (81.5 %) -- aquatic:marine
      0 / 64    (   0 %) -- aquatic:saline
     58 / 94    (61.7 %) -- host-associated:animal host:digestive tract:intestine
     49 / 80    (61.3 %) -- host-associated:animal host:digestive tract:mouth
     51 / 82    (62.2 %) -- host-associated:animal host:digestive tract:mouth:saliva
    103 / 114   (90.4 %) -- host-associated:animal host:digestive tract:rumen
      0 / 40    (   0 %) -- host-associated:animal host:respiratory tract
      0 / 47    (   0 %) -- host-associated:animal host:skin
     26 / 74    (35.1 %) -- host-associated:plant host:aereal
     38 / 74    (51.4 %) -- host-associated:plant host:rhizosphere
      0 / 1     (   0 %) -- terrestrial:dust
     18 / 43    (41.9 %) -- terrestrial:permafrost
     25 / 68    (36.8 %) -- terrestrial:soil
Final accuracy: 56.5 %
```


## Listing the labels

To see which labels are included in the samples, you can run `list_labels.py`:

```
$ ./list_labels.py
Labels (and number of entries) present in data/umap_subsampling_1_10_umap_labels.csv :
    603 -- aquatic:freshwater
    739 -- aquatic:marine
    266 -- aquatic:saline
    395 -- host-associated:animal host:digestive tract:intestine
    349 -- host-associated:animal host:digestive tract:mouth
    331 -- host-associated:animal host:digestive tract:mouth:saliva
    426 -- host-associated:animal host:digestive tract:rumen
    166 -- host-associated:animal host:respiratory tract
    190 -- host-associated:animal host:skin
    328 -- host-associated:plant host:aereal
    324 -- host-associated:plant host:rhizosphere
      5 -- terrestrial:dust
    187 -- terrestrial:permafrost
    286 -- terrestrial:soil
```

Knowing the labels, you can train the neural network to compare one
class (one biome) to the rest, using the `--singled-label` option. For
example:

```sh
$ ./biomes_nn.py -q -e 500 --singled-label aquatic:marine
Correct predictions:
     82 / 210   (  39 %) -- aquatic:marine
    926 / 939   (98.6 %) -- other
Final accuracy: 87.7 %
```


## Tests

You can run some simple tests from the `tests` directory with
[pytest](https://docs.pytest.org/), by simply running `pytest` from
the main directory.
