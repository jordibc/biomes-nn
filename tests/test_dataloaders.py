"""
Functions to test the correct working of the neural network classifier.

Use pytest to run them.
"""

import sys
sys.path += ['.', '..']  # so we can run pytest in this directory or the parent

from tempfile import NamedTemporaryFile
import numpy as np

import dataloaders


example_labels = """\
labels
host-associated:plant host:aereal
host-associated:animal host:skin
aquatic:freshwater
host-associated:plant host:aereal
host-associated:animal host:skin
host-associated:plant host:rhizosphere
aquatic:freshwater
host-associated:plant host:aereal
host-associated:plant host:aereal
"""

example_umaps = """\
umap_1,umap_2,umap_3,umap_4,umap_5,umap_6
14.752,-5.809,18.367,12.811,10.35,7.6644
15.415,-5.270,17.591,13.164,10.875,8.356
15.505,-6.691,18.853,13.215,9.072,7.8103
15.412,-4.039,18.141,13.371,10.829,7.627
15.254,-5.097,18.336,13.798,9.5895,7.979
14.958,-5.679,18.544,12.298,10.88,7.2767
17.999,-5.385,17.784,13.130,10.844,8.631
15.369,-5.633,18.916,14.003,10.716,7.507
15.638,-5.082,18.064,14.956,10.202,7.557
"""


def are_equal(a, b):
    """Return True if the two given lists of floats are approximately equal."""
    EPSILON = 1e-5
    return (len(a) == len(b) and
            all(abs(ai - bi) < EPSILON for ai, bi in zip(a, b)))


def test_dataloaders():
    with (NamedTemporaryFile('wt') as f_umaps,
          NamedTemporaryFile('wt') as f_labels):
        f_umaps.write(example_umaps)
        f_umaps.flush()
        f_labels.write(example_labels)
        f_labels.flush()

        # Unshuffled.
        data_train, data_test = dataloaders.get_dataloaders(
            f_umaps.name, f_labels.name, test_size=0.25, shuffle=False)

        # Shuffled.
        np.random.seed(7)  # fix random seed for the test
        data_train_rnd, data_test_rnd = dataloaders.get_dataloaders(
            f_umaps.name, f_labels.name, test_size=0.25, shuffle=True)

        # Selecting a single label.
        data_train_skin, data_test_skin = dataloaders.get_dataloaders(
            f_umaps.name, f_labels.name, test_size=0.25, shuffle=False,
            singled_label='host-associated:animal host:skin')


    assert len(data_train.dataset) == 6
    assert len(data_test.dataset) == 3

    assert are_equal(data_train.dataset[0][0].tolist(),
                     [14.752, -5.809, 18.367, 12.811, 10.35, 7.6644])
    assert data_train.dataset[0][1] == 2

    assert data_train.dataset.labels[0] == 'aquatic:freshwater'

    # The same for the shuffled version.

    assert len(data_train_rnd.dataset) == 6
    assert len(data_test_rnd.dataset) == 3

    assert are_equal(data_train_rnd.dataset[0][0].tolist(),
                     [15.505, -6.691, 18.853, 13.215, 9.072, 7.8103])
    assert data_train_rnd.dataset[0][1] == 0

    assert data_train_rnd.dataset.labels[0] == 'aquatic:freshwater'

    # The same for the singled-label version.

    assert len(data_train_skin.dataset) == 6
    assert len(data_test_skin.dataset) == 3

    assert are_equal(data_train_skin.dataset[0][0].tolist(),
                     [14.752, -5.809, 18.367, 12.811, 10.35, 7.6644])
    assert data_train_skin.dataset[0][1] == 1

    assert (data_train_skin.dataset.labels ==
            ['host-associated:animal host:skin', 'other'])


def test_good_markers():
    xs = np.array([[1, 0, 2, 0, 3],
                   [0, 0, 4, 5, 0],
                   [6, 0, 7, 0, 0]])
    #                  ^-- this column (1) is all zeros -- not a good marker

    assert dataloaders.get_good_markers(xs).tolist() == [0, 2, 3, 4]
